use std::ffi::CString;
use std::os::raw::c_ulong;

use std::ptr;
use x11::xlib;

pub struct X11Interface {
    display: *mut xlib::Display,
    window: c_ulong,
}

impl X11Interface {
    pub fn new() -> Self {
        let display = unsafe { xlib::XOpenDisplay(ptr::null()) };
        let window = unsafe { xlib::XDefaultRootWindow(display) };

        Self { display, window }
    }

    pub fn set_status(&self, data: &str) {
        let cstr = CString::new(data).unwrap();
        let ptr = cstr.as_ptr() as *const i8;

        unsafe {
            xlib::XStoreName(self.display, self.window, ptr);
            xlib::XSync(self.display, 0);
        }
    }
}

impl Drop for X11Interface {
    fn drop(&mut self) {
        unsafe {
            xlib::XCloseDisplay(self.display);
        }
    }
}
