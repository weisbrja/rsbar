use std::process::Command;

#[derive(PartialEq, Eq, Hash)]
pub struct Block {
    pub cmd: &'static str,
    pub interval: Option<u32>,
    pub signum: Option<i32>,
}

impl Block {
    pub fn new(cmd: &'static str, interval: Option<u32>, signum: Option<i32>) -> Self {
        Self {
            cmd,
            interval,
            signum,
        }
    }

    pub fn exec(&self) -> String {
        String::from_utf8(
            Command::new("/bin/sh")
                .arg("-c")
                .arg(&self.cmd)
                .output()
                .unwrap()
                .stdout,
        )
        .unwrap()
    }
}
