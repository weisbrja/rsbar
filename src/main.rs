mod block;
mod x11_interface;

use std::time::Duration;

use block::Block;
use x11_interface::X11Interface;

use tokio::signal::unix::SignalKind;
use tokio::time::Instant;

use futures::StreamExt;
use tokio_stream::wrappers::SignalStream;

const SEPARATOR: &str = "^d^|";

#[tokio::main(worker_threads = 1)]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let x11_interface = X11Interface::new();

    let blocks = [
        Block::new("bar_xautolock", None, Some(1)),
        Block::new("bar_network", Some(30), None),
        Block::new("bar_keyboard_layout", None, Some(2)),
        Block::new("bar_volume", None, Some(3)),
        Block::new("bar_memory", Some(30), None),
        Block::new("bar_date", Some(60), None),
        Block::new("bar_battery", Some(5), None),
    ];

    let mut cache = Vec::with_capacity(blocks.len());

    let sigrtmin = libc::SIGRTMIN();
    let mut signal_stream = futures::stream::select_all(
        blocks
            .iter()
            .filter_map(|block| block.signum)
            .map(|signum| {
                SignalStream::new(
                    tokio::signal::unix::signal(SignalKind::from_raw(sigrtmin + signum)).unwrap(),
                )
                .zip(futures::stream::repeat(signum))
            }),
    );

    let mut tick = Instant::now();
    let second = Duration::from_secs(1);
    loop {
        match tokio::time::timeout_at(tick, signal_stream.next()).await {
            Ok(Some(((), signum))) => {
                for (i, block) in blocks
                    .iter()
                    .enumerate()
                    .filter(|(_, block)| block.signum == Some(signum))
                {
                    cache[i] = block.exec();
                }
                let status = cache.join(SEPARATOR);
                x11_interface.set_status(&status);
            }
            Err(_) => {
                let new = blocks.iter().map(Block::exec).collect();
                if new != cache {
                    cache = new;
                    let status = cache.join(SEPARATOR);
                    x11_interface.set_status(&status);
                }
                tick += second;
            }
            Ok(None) => unreachable!(),
        }
    }
}
